<?php

namespace laylatichy\nano\core\config;

use Dotenv\Dotenv;

class Config {
    private static Config $_instance;

    private function __construct(
        private Environment $mode = Environment::DEV,
        private string $host = 'http://0.0.0.0',
        private int $port = 3000,
        private string $baseUrl = '//',
        private string $root = '.',
    ) {}

    public static function getInstance(): self {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getMode(): Environment {
        return $this->mode;
    }

    public function withMode(Environment $mode): self {
        $this->mode = $mode;

        return $this;
    }

    public function getHost(): string {
        return $this->host;
    }

    public function withHost(string $host): self {
        $this->host = $host;

        return $this;
    }

    public function getPort(): int {
        return $this->port;
    }

    public function withPort(int $port): self {
        $this->port = $port;

        return $this;
    }

    public function getBaseUrl(): string {
        return $this->baseUrl;
    }

    public function withBaseUrl(string $baseUrl): self {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function getRoot(): string {
        return $this->root;
    }

    public function withRoot(string $root): self {
        $this->root = $root;

        return $this;
    }

    public function load(): self {
        if (file_exists($this->root . '/../.env.' . $this->mode->value)) {
            Dotenv::createImmutable($this->root, '/../.env.' . $this->mode->value)->load();
        }

        $file = $this->root . '/../.config/config.php';

        if (file_exists($file)) {
            require $file;
        }

        return $this;
    }
}
