<?php

use laylatichy\nano\core\config\Config;

if (!function_exists('useConfig')) {
    function useConfig(): Config {
        return Config::getInstance();
    }
}