<?php

namespace laylatichy\nano\core\config;

enum Environment: string {
    case DEV        = 'dev';
    case STAGING    = 'staging';
    case TEST       = 'test';
    case CI         = 'ci';
    case PEST       = 'pest';
    case PRODUCTION = 'production';
}
