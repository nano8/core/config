---
layout: home

hero:
    name:    nano/core/config
    tagline: configuration module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/core/config is a configuration module for nano
    -   title:   configuration files
        details: |
                 nano/core/config provides a simple way to load configuration files
---
