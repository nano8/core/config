---
layout: doc
---

<script setup>
const args = {
    useConfig: [],
    getMode: [],
    withMode: [
        {
            type: 'Environment',
            name: 'mode',
        },
    ],
    getHost: [],
    withHost: [
        {
            type: 'string',
            name: 'host',
        },
    ],
    getPort: [],
    withPort: [
        {
            type: 'int',
            name: 'port',
        },
    ],
    getBaseUrl: [],
    withBaseUrl: [
        {
            type: 'string',
            name: 'baseUrl',
        },
    ],
    getRoot: [],
    withRoot: [
        {
            type: 'string',
            name: 'root',
        },
    ],
    load: [],
};
</script>

##### laylatichy\nano\core\config\Config

## usage

`useConfig()` is a helper function to get the config instance

it's fully typed and chainable

config file from `root . '/../.config/config.php'`
will be loaded automatically on the nano start

```php
useConfig()
    ->withMode(Environment::DEV)
    ->withHost('http://localhost')
    ->withPort(3000);
```

## <Types fn="useConfig" r="Config" :args="args.useConfig" /> {#useConfig}

```php
useConfig();
```

defaults

```php
mode: Environment::DEV
host: 'http://0.0.0.0'
port: 3000
baseUrl: '//'
root: '.'
```

## <Types fn="getMode" r="Environment" :args="args.getMode" /> {#getMode}

```php
useConfig()->getMode();
```

## <Types fn="withMode" r="Config" :args="args.withMode" /> {#withMode}

```php
useConfig()->withMode(Environment::DEV);
```

## <Types fn="getHost" r="string" :args="args.getHost" /> {#getHost}

```php
useConfig()->getHost();
```

## <Types fn="withHost" r="Config" :args="args.withHost" /> {#withHost}

```php
useConfig()->withHost('http://localhost');
```

## <Types fn="getPort" r="int" :args="args.getPort" /> {#getPort}

```php
useConfig()->getPort();
```

## <Types fn="withPort" r="Config" :args="args.withPort" /> {#withPort}

```php
useConfig()->withPort(3000);
```

## <Types fn="getBaseUrl" r="string" :args="args.getBaseUrl" /> {#getBaseUrl}

```php
useConfig()->getBaseUrl();
```

## <Types fn="withBaseUrl" r="Config" :args="args.withBaseUrl" /> {#withBaseUrl}

```php
useConfig()->withBaseUrl('https://example.com');
```

## <Types fn="getRoot" r="string" :args="args.getRoot" /> {#getRoot}

```php
useConfig()->getRoot();
```

## <Types fn="withRoot" r="Config" :args="args.withRoot" /> {#withRoot}

set root directory, will be used to load config file

```php
useConfig()->withRoot('src');
```

## <Types fn="load" r="Config" :args="args.load" /> {#load}

and env config file from `root . '/../.env.{mode}'`

load config file from `root . '/../.config/config.php'`

```php
useConfig()->load();
```
