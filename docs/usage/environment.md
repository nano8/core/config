---
layout: doc
---

##### laylatichy\nano\core\config\Environment

## usage

Environment is an enum class that contains the available environments

```php
use laylatichy\nano\core\config\Environment;

Environment::DEV
```

## available environments

```php
Environment::DEV
Environment::STAGING
Environment::TEST
Environment::PRODUCTION
```





